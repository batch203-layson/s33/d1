

/*
[SECTION] JavaScript Synchronous vs Asynchronus

JavaScript is by default synchronous meaning that it only executes one statement at a time



*/



console.log("Hello Universe");

//consle.log("Hello Again");

console.log("Good bye");

/*

Code Blocking - Waiting for the specific statement to finish before executing the next statement

for(let i = 0 ; i <= 1500; i++)
{
	console.log(i);
}

*/
console.log("Hello there");

//Asynchronus means that we can proceed to execute other statements, while time consuming code is running in the background

/*
[SECTION] Getting all posts

the fetch API that allows us to asynchronously request for a resource (data)

"fetch()" method  in JavaScript is used to request to the server and load information on the webpages

syntax:

fetch("apiURL");

A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value.

A promise may be in one of 3 possible states:

fulfilled

rejected

pending



Fulfilled
-operation was completed

Rejected
-Operation failed


syntax
fetch("apiURL")
.then((response)=>{})


the ".then()" method captures the response object and returns another promise which will be either "resolved" or "rejected".
*/



fetch("https://jsonplaceholder.typicode.com/posts")
//the ".then()" method captures the response object and returns another promise which will be either "resolved" or "rejected".
// .then(response => console.log(response.status));

// ".json()" method will convert JSON format to JavaSCript object
.then(res  => res.json())
// we can now manipulate or use the converted response in our code
// .then(json => console.log(json));
/*

We can use array methods to display each post title
.then(json =>{
	json.forEach(post => console.log(post.title));
});


The "async" and "await" keyword to achieve asynchronous code



*/


async function fetchData(){
	let result = await(fetch("https://jsonplaceholder.typicode.com/posts"));
	// Returned the "Response" object
	console.log(result);

	let json = await result.json();

	console.log(json);
}

fetchData();



/*

[SECTION] Getting a specific post

*/

/**/
fetch("https://jsonplaceholder.typicode.com/posts/1")
.then(response=>response.json())
.then(json => console.log(json));



/*
[SECTION] Creating a post

syntax

fetch("apiURL", options)
.then((response) =>{})
.then((response) =>{})
*/

fetch("https://jsonplaceholder.typicode.com/posts",
		{
			method: "POST",
			headers: {
				"Content-Type":"application/json"
			},
		
		body: JSON.stringify({
			title: "New Post",
			body: "Hellow Universe!",
			userId:1
		})
	}
	)
	.then(response=>response.json())
	.then(json=>console.log(json));


/*
	[SECTION] Updating a post


*/

fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "New Post",
			body: "Hello again Universe!",
			userId:1
		})

	})
	.then(response=>response.json())
	.then(json=>console.log(json));

/*
[SECTION] Updating a post using PATCH Method

PUT vs PATCH

PATCH is used to update a single or several properties

PUT is used to update the while document 
overwrite
*/


fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Corrected post title"
		})

	})
	.then(response=>response.json())
	.then(json=>console.log(json));

/*
[SECTION] Delte a post


*/

fetch("https://jsonplaceholder.typicode.com/posts/1", {method: "DELETE"})
.then(response=>response.json())
.then(json=>console.log(json));